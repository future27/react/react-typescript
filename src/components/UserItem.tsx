import React from 'react';
import { IUser } from "../types/types";


interface UserItemProps {
    user: IUser;
    onClick: (user: IUser) => void;
}
const UserItem: React.FC<UserItemProps> = ({ user, onClick }) => {
    return (
        <div onClick={() => onClick(user)} key={user.id} style={{ padding: 15, border: "1px solid gray", cursor: "pointer" }}>
            {user.id}. {user.name} living in {user.address.city} on street {user.address.street}

        </div>
    );
};

export default UserItem;
